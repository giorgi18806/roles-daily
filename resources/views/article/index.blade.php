@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Articles</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ route('articles.create') }}" class="btn btn-success">New Article</a>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <th>Title</th>
                                        <th>Created At</th>
                                    </thead>
                                    <tbody>
                                        @forelse($articles as $article)
                                           <tr>
                                               <td>{{ $article->title }}</td>
                                               <td>{{ $article->created_at }}</td>
                                           </tr>
                                        @empty
                                            <tr>
                                                <td colspan="2"><h5 class="text-center">No articles found.</h5></td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
