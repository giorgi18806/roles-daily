@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">New Article</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('articles.store') }}" method="POST">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">
                                    </div>
                                    <div class="mb-3">
                                        <label for="full_text">Full text</label>
                                        <textarea class="form-control" name="full_text" id="full_text" cols="30" rows="10">{{ old('full_text') }}</textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="Category">Category</label>
                                        <select class="form-control" name="category_id" id="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
